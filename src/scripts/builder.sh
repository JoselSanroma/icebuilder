#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-


	# red="\e[1;31m"
	# green="\e[1;32m"
	# yellow="\e[1;33m"
	# blue="\e[1;34m"
	# norm="\e[0m"
	# color=""



#                    |amd64|
#build_package.sh -a |i386 | package_name
#                    |all  |

export LIBVIRT_DEFAULT_URI=qemu:///system

#i386  ---> 192.168.122.11
#amd64 ---> 192.168.122.12

HOST1="192.168.122.11"
HOST2="192.168.122.12"
USER="arco"
PASS="arco"
PACKAGE=$3
DIRSRC=""

#date +'%b %e %H:%M:%S'


#VM NAMES
i386=bully-i386
amd64=rigodon-amd64


update="sudo aptitude update;"

upgrade="sudo aptitude safe-upgrade -y";

sourcep="apt-get source $PACKAGE -y";

buildDep="sudo apt-get build-dep $PACKAGE -y";

directory="ls -F | grep /"

# build="cd $DIRSRC;\
#        debuild -us -uc";


function build_help {
    cat <<EOF
HELP
-----------------
sh build_package.sh -a arch op package

arch={i386,amd64}
op indicates the operation :{update, upgrade,buildDep,build,..}
package is the package name to build

Examples
---------
build_package.sh i386 update
build_package.sh amd64 build sl

EOF
}

function check_state (){
    st=true
    while $st ; do
        if $(ping -c3 -i 3 $1 > /dev/null); then
            st=false
        fi
    done
}

function start_machines(){

    if [ "$(virsh domstate $1)" = "shut off" ]; then
        echo apagada
        virsh start $1
    else
        echo encendida
    fi
}

function create_snapshot(){

    echo "Creating snapshot..."
    sudo virsh snapshot-create-as $1 $2
    echo "snapshots created...[ OK ]"
}

function delete_snapshot(){

    echo "deleting snapshot..."
    virsh snapshot-delete $1 $2
    echo "snapshots deleted...[ OK ]"
}

function revert_snapshot(){
    echo $1
    echo $2

    echo "Reverting snapshot..."
    virsh snapshot-revert $1 $2
    echo "snapshots reverted...[ OK ]"
}

function build_operation(){

    HOST=$HOST1
    VM=$i386
    if [ "$1" = "amd64" ]
    then
        HOST=$HOST2
        VM=$amd64
    fi

    echo $HOST $VM

    case $2 in

        start)
            echo start
            start_machines $VM
            check_state $HOST
            ;;
        update)
            echo update
            sshpass -p 'arco' ssh -o StrictHostKeyChecking=no $USER@$HOST $update
            ;;

        upgrade)
            echo upgrade
            sshpass -p 'arco' ssh -o StrictHostKeyChecking=no $USER@$HOST $upgrade
            ;;

        createSnapshot)
            echo create_snapshot
            create_snapshot $VM $3
            ;;

        sourcePackage)
            echo source
            sshpass -p 'arco' ssh -o StrictHostKeyChecking=no $USER@$HOST $sourcep
            ;;

        buildDependencies)
            echo buildDep
            sshpass -p 'arco' ssh -o StrictHostKeyChecking=no $USER@$HOST $buildDep
            ;;

		# sourceDirectory)
		# 	echo dir
		# 	DIRSRC="$(sshpass -p 'arco' ssh -o StrictHostKeyChecking=no $USER@$HOST $directory)"
		# 	;;

        buildPackage)
            echo build
			DIRSRC="$(sshpass -p 'arco' ssh -o StrictHostKeyChecking=no $USER@$HOST $directory)"
			build="cd $DIRSRC;\
                        debuild -us -uc";

			echo $build
            sshpass -p 'arco' ssh -o StrictHostKeyChecking=no $USER@$HOST $build
            ;;

        revertSnapshot)
            echo revert
            revert_snapshot $VM $3
            ;;

        deleteSnapshot)
            echo delete_snapshot
            delete_snapshot $VM $3
            ;;

        turnOff)
            echo turnOff
            virsh shutdown $VM
            ;;

        *);;
    esac
}



# choice(){
#     if [ -z $4 ]
#     then
#         echo "packet expected!"
#     else
#         if [ "$1" = "-a" ]
#         then
#             if [ "$2" = "amd64" ]
#             then
#                 echo "Llamar build_amd64"
#                 start_machines $amd64
#                 build_amd64
#             else
#                 if [ "$2" = "i386" ]
#                 then
#                     #start_machines $i386
#                     build_i386p $2 $3 $4
#                 else
#                     if [ "$2" = "all" ]
#                     then
#                         echo "todas las arquitecturas"
#                         start_machines $i386 & start_machines $amd64
#                         build_i386 & build_amd64
#                     else
#                         echo "valid arch expected!"
#                     fi
#                 fi
#             fi
#         else
#             echo "-a expected!"
#         fi
#     fi
# }


if [ "$1" = "help" ]
then
    build_help
else

    build_operation $1 $2 $3
fi



exit 0
