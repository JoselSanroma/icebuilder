#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-

PACKAGE=$1

# sed sed "1d"  quita la primera linea
# sed 's/^ *//g' quita espacios en blanco
# sed 's/Build-Depends: //g' elimina las cadenas Build-Depends
# sed 's/Build-Depends: \|Build-Depends-Indep: //g' elimina las cadenas Build-Depends y Build-Depends-Indep

# sed 's/Build-Depends-Indep: //g' elimina las cadenas Build-Depends

# sed s'/(.*)//g' quitar texto entre paréntesis
# sed '/^$/ d' quitar lineas en blanco
# http://www.sromero.org/wiki/linux/aplicaciones/uso_de_sed#ejemplos_de_borrado


#apt-rdepends --build-depends -f=depends $1 | sed "1d" | sed 's/^ *//g' | sed 's/Build-Depends: //g' > $1_depends

apt-rdepends --build-depends -f=depends $1 | sed "1d" | sed 's/^ *//g' | sed 's/Build-Depends: \|Build-Depends-Indep: //g' | sed s'/(.*)//g' > $1
