#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-

#arch={i386, amd64, armel}

#check_depends arch package

arch_list="i386 amd64"

arch=$1
package=$2
distribution=$3


function listcontains() {
  list=$1
  item=$2

  if [[ $list =~ (^|[[:space:]])"$item"($|[[:space:]]) ]] ; then
    # yes, list include item
    result=0
  else
    result=1
  fi
  return $result
}

function download_repo(){
    wget http://arco.esi.uclm.es/~joseluis.sanroma/debian/dists/sid/main/binary-$arch/Packages.gz --output-document=repo_binary_$arch.gz
    wget http://ftp.us.debian.org/debian/dists/$distribution/main/binary-$arch/Packages.gz --output-document=debian_binary_$arch.gz
    wget http://arco.esi.uclm.es/~joseluis.sanroma/debian/dists/sid/main/source/Sources.gz

}


function dose(){
    dose-builddebcheck -s -f --checkonly=$package \
    --deb-native-arch=$arch \
    repo_binary_$arch.gz \
    debian_binary_$arch.gz \
    Sources.gz
}

if listcontains "$arch_list" "$arch"
then
    echo hola
    download_repo
    dose > temp_$package.yaml

	sed 's/^ *//g' $"temp_$package.yaml" | sed '1,3d' | sed '/^$/ d' | sed '7,$d' > $package.yaml


else
    echo "architecture does not exist!"
fi



# FLAG=0
# if [ "$(listcontains "$arch_list" "$arch")" ]
# then
#     echo hola
# else
#     echo adios
# fi






# listcontains "$arch_list" "$arch" && echo "yes" || echo "no"
# echo $?
