// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include <Ice/Ice.h>

#include <CommandType.h>
#include <CommandTypeI.h>


using namespace std;
using namespace Ice;

using namespace CommandType;



class Server: public Ice::Application {
public:

  void interruptCallback(int i){
    std::cout << "Interrupción!" << std::endl;
    _exit(i);
  }
  
  int run(int argc, char* argv[]) {
    //shutdownOnInterrupt();

    callbackOnInterrupt();



    
    Ice::ObjectFactoryPtr factory = new CommandFactory;

    communicator()->addObjectFactory(factory, SimpleCommand::ice_staticId());
    communicator()->addObjectFactory(factory, CompositeCommand::ice_staticId());
    
    Ice::ObjectAdapterPtr adapter=communicator()
      ->createObjectAdapterWithEndpoints("EvictorDebBuilder","tcp -p 8888");



    Freeze::TransactionalEvictorPtr evictor=
      Freeze::createTransactionalEvictor(adapter,"db","evictorDeb",
                                         Freeze::FacetTypeMap(), new CompositeInitializer());
            
    CommandType::SimpleCommandI::simpleEvictor=evictor;
    CommandType::CompositeCommandI::compositeEvictor=evictor;
    CommandType::CompositeInitializer::initializeEvictor=evictor;

    adapter->addServantLocator(evictor,"");


    // Ice::Identity compositeId;
    // compositeId.name="MainComposite";

    Ice::Identity compositeId=communicator()->stringToIdentity("MainComposite");
    CommandType::CompositeCommandPrx prx;
    prx=CommandType::CompositeCommandPrx::uncheckedCast(adapter->createProxy(compositeId));

    if (evictor->hasObject(compositeId)){
      Freeze::EvictorIteratorPtr p=evictor->getIterator("",20);
      while(p->hasNext()){
        Ice::Identity ident=p->next();
        std::cout << "HasObject identName: " << ident.name << std::endl;
      }
      std::cout << compositeId.name << " ya estaba en este evictor" << std::endl;
      // std::cout << "previous package-->" << prx->getPackage() << std::endl;
      // prx->execute();
      prx->buildDFS();

    }
    else{
      
      Ice::ObjectPrx obj = evictor->add(new CompositeCommandI, compositeId);     
      std::cout << communicator()->proxyToString(obj) << std::endl;
    }


    adapter->activate();
    communicator()->waitForShutdown();
    
    std::cout << "Done" << endl;

    return 0;
  }
};



int main(int argc, char* argv[]) {
  Server app;
  return app.main(argc, argv);
}

