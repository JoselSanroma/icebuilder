// -*- coding:utf-8; tab-width:4; mode:cpp -*-
#include <unistd.h>
#include <stdlib.h>
#include <list>
#include <fstream>
#include <iostream>

using namespace std;

bool is_empty(std::ifstream& pFile)
{
    return pFile.peek() == std::ifstream::traits_type::eof();
}

int main(int argc, char *argv[])
{
  std::string path="../scripts/";
  std::string script="build_depends_list.sh";
  std::string command;
  command.append(path);
  command.append(script+" ");
  command.append(argv[1]);
  system(command.c_str());


  std::ifstream fe(argv[1]);

  std::list<string> tempDependencies;


  if (!is_empty(fe)){
    std::string dep;
    while(!fe.eof()){
      getline(fe,dep);
      tempDependencies.push_back(dep);
    }
  }

  if(tempDependencies.empty()){
    std::cout << "size " << tempDependencies.size();
  }
  else{
    std::cout << "NO" << std::endl;
  }
  // tempDependencies.pop_back();


  return 0;
}


// // -*- coding:utf-8; tab-width:4; mode:cpp -*-
// #include <iostream>     // std::cin, std::cout
// #include <string>
// #include <iostream>
// #include <fstream>
// #include <vector>

// using namespace std;




// int main () {
//   string paquete;
//   char cadena[128];
//   std::ifstream fe("zeroc");

//   vector<string> v;

//   while (!fe.eof()){
//     getline(fe,paquete);
//     v.push_back(paquete);
//   }
//   // cout << v.size() << endl;
//   // getline(fe,paquete);
//   // cout << paquete << endl;

//   std::vector<string>::iterator it= v.begin();
//   int count =0;
//   while(it!=v.end()){
//     std::cout << (*it++) << std::endl;
//     count+=1;
//   }

//   std::cout << count-1 << std::endl;


//   // while (!fe.eof()){
//   //   fe >> cadena;
//   //   v.push_back(cadena);
//   //   std::cout << v.size() << std::endl;
//   //   // std::cout << cadena << std::endl;
//   // }



//   return 0;
// }

  // char name[256], title[256];


  // std::cout << "Please, enter your name: ";
  // std::cin.getline (name,256);

  // // std::string name2=std::string(name);
  // // std::string title2=std::string(title);

  // std::cout << "Please, enter your favourite movie: ";
  // std::cin.getline (title,256);

  // std::cout << name << "'s favourite movie is " << title << std::endl;

  // // std::cout << name2 << "'s favourite movie is " << title2 << std:: endl;
