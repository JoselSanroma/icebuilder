// -*- coding:utf-8; tab-width:4; mode:cpp -*-

#ifndef __CommandTypeI_h__
#define __CommandTypeI_h__

#include <CommandType.h>
#include <DebBuilderI.h>

#include <unistd.h>
#include <vector>
#include <queue>
#include <list>
#include <IceUtil/IceUtil.h>
#include <Freeze/Freeze.h>
#include <yaml-cpp/yaml.h>
/* #include <FileOperations.h> */

using namespace DebBuilder;
/* using namespace FileOperations; */



namespace CommandType{

  class CompositeCommandI;

  class ConcreteCommandI : virtual public ConcreteCommand,
                           virtual public ::DebBuilder::CommandI{
  public:
    ConcreteCommandI();
    virtual void execute(const Ice::Current&);
    /* virtual void destroy(const Ice::Current&); */

   };



  class SimpleCommandI : virtual public SimpleCommand,
                         virtual public ::CommandType::ConcreteCommandI{

  public:
    SimpleCommandI();
    /* SimpleCommandI(std::string&); */
    /* SimpleCommandI(std::string&, std::string&); */
    /* SimpleCommandI(std::string&, std::string, std::string&); */
    /* SimpleCommandI(std::string, std::string, std::string, std::string); */
    SimpleCommandI(const ::DebBuilder::Package&, const std::string&, const std::string&);
    ~SimpleCommandI()=default;

    virtual void execute(const Ice::Current&);
    //    virtual void destroy(const Ice::Current&);

    static Freeze::TransactionalEvictorPtr simpleEvictor;
  };



  class CompositeCommandI : virtual public CompositeCommand,
                            virtual public ::CommandType::ConcreteCommandI,
                            public IceUtil::AbstractMutexI<IceUtil::Mutex>{

  friend class CompositeInitializer;
  friend class CommandFactory;

  public:
    CompositeCommandI();
    CompositeCommandI(const ::DebBuilder::Package&);
    ~CompositeCommandI();

    /* //virtual void setPackage(const ::DebBuilder::Package&, const Ice::Current&); */
    virtual void add(const ::CommandType::ConcreteCommandPtr&,
                     const Ice::Current&);

    /* /\* ::std::string getPackage(const Ice::Current&); *\/ */

    virtual void init(const ::DebBuilder::Package&, const Ice::Current&);


    /* virtual ::DebBuilder::PackageList */
    virtual Ice::Int
      generateDependencyTree(const ::DebBuilder::Package&,
			     ::Ice::Int depth,
			     const Ice::Current&);

    virtual ::DebBuilder::PackageList
      generateDependencies(const ::DebBuilder::Package&,const Ice::Current&);

    virtual bool buildable(const ::DebBuilder::Package& p, const Ice::Current& current);

    /* /\* virtual void build(const ::DebBuilder::Package&, const Ice::Current&); *\/ */
    /* virtual void buildDFS(const Ice::Current&); */
    /* virtual CompositeCommandPrx getComposites(const Ice::Current&); */
    virtual void execute(const Ice::Current&);
    virtual ::CommandType::CompositeCommandPrx createComposite(const ::DebBuilder::Package&,const Ice::Current&);

    virtual ::CommandType::CompositeSeq reverseDFS(::Ice::Int,
						   ::CommandType::CompositeSeq&,
						   const ::Ice::Current&);
    virtual ::CommandType::CompositeSeq getParents(const ::Ice::Current&);





    /* virtual ::CommandType::CompositeCommandPtr reverseDFS(::Ice::Int, const CompositeCommandPrx&, const Ice::Current&); */


    /* virtual void destroy(const Ice::Current&); */
    /* virtual void removeComposite(const Ice::Current&); */

    static Freeze::TransactionalEvictorPtr compositeEvictor;
    /* //    static Ice::ObjectAdapterPtr oa; */


  private:
    IceUtil::Mutex compositeMutex;
    bool _destroy;
  };



  typedef IceUtil::Handle<CompositeCommandI> CompositeCommandIPtr;


  class CompositeFactory : public Ice::ObjectFactory{
  public:
    virtual Ice::ObjectPtr create(const std::string&);
    virtual void destroy();
  };




  class CompositeInitializer : public Freeze::ServantInitializer{
  public:
    virtual void initialize(const Ice::ObjectAdapterPtr&,
                            const Ice::Identity&,
                            const std::string&,
                            const Ice::ObjectPtr&);
    static Freeze::TransactionalEvictorPtr initializeEvictor;

  };

}

#endif
