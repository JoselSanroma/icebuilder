#include <DebBuilder.ice>
#include <Ice/Identity.ice>

// [["cpp:include:queue"]]

module CommandType{

  class CompositeCommand;


  class ConcreteCommand implements DebBuilder::Command{
    // // Ice::Identity id;
    // CompositeCommand* parent;
    DebBuilder::Package debPackage;
  };

  ["cpp:virtual"]
  class SimpleCommand extends ConcreteCommand{
    // string architecture;
    string operation;
    // string packageName;
    string snapshotName;
  };


  sequence<ConcreteCommand> CommandSeq;

  ["cpp:type:std::list<::CommandType::CompositeCommandPtr>"]
  sequence<CompositeCommand> CompositeSeq;

  ["cpp:type:std::list<::CommandType::CompositeCommandPrx>"]
  sequence<CompositeCommand*> CompositePrxList;


  ["cpp:virtual"]
  class CompositeCommand extends ConcreteCommand{
    CommandSeq commands;
    CompositePrxList buildDepends;
    CompositePrxList parents;
    //DebBuilder::Package debPackage;

    //methods
    ["freeze:write"]void init(DebBuilder::Package p);

    ["freeze:write"]void add(ConcreteCommand cc);

    // void buildDFS();
    int generateDependencyTree(DebBuilder::Package p, int depth);

    DebBuilder::PackageList generateDependencies(DebBuilder::Package p);
    bool buildable(DebBuilder::Package p);
    ["freeze:write"]CompositeCommand* createComposite(DebBuilder::Package p);

    CompositeSeq reverseDFS(int depth, out CompositeSeq compositePackages);
    CompositeSeq getParents();



    // void removeComposite();
    // ["freeze:write"]void destroy();
  };
};
