// -*- coding:utf-8; tab-width:4; mode:cpp -*-

// #include <IceUtil/IceUtil.h>
// #include <Freeze/Freeze.h>
#include <Ice/Ice.h>
// #include <DebBuilder.h>
// #include <DebBuilderI.h>
#include <CommandType.h>

using namespace std;
using namespace DebBuilder;

class ManagerCB: public IceUtil::Shared{
public:
  void response(const Ice::Long retval){
    std::cout << "Callback: Value is " << retval << std::endl;
  }
  void failure(const Ice::Exception& ex){
    std::cout << "Exception is: " << ex << std::endl;
  }
};

class Client: public Ice::Application {
public:

  virtual int run (int argc, char* argv[]) {

    //proxy oneway
    Ice::ObjectPrx obj = communicator()->stringToProxy(argv[1]);

    //one way proxy
    Ice::ObjectPrx oneway;

    try{
      oneway=obj->ice_oneway();
    }catch (const Ice::NoEndpointException&){
      cerr << "No endpoint for oneway invocation" << endl;
    }


    //downCast
    DebBuilder::ManagerPrx onewayManager;
    onewayManager=DebBuilder::ManagerPrx::uncheckedCast(oneway);

    //twoways
    DebBuilder::ManagerPrx two;
    two=DebBuilder::ManagerPrx::checkedCast(obj);




    //    CommandType::CompositeCommandPrx twoways=CommandType::CompositeCommandPrx::checkedCast(obj);


    Package p1 {argv[2], argv[3], "1.0",needsBuild};
    // Package p1;
    // p1.name=argv[2];
    // p1.arch=argv[3];
    // p1.version="1.0";
    // p1.state=needsBuild;

    std::cout << "Package name: " << p1.name << std::endl;
    std::cout << "Package arch: " << p1.architecture << std::endl;
    std::cout << "Package arch: " << p1.state << std::endl;


    //package building
    // prx->setPackage(p1);

    //async
    // Ice::AsyncResultPtr async=prx->begin_setPackage(p2);
    // std::cout << "async resut" << std::endl;
    // prx->end_setPackage(async);

    // prx->getPackage();
    try{

      onewayManager->build(p1);


    } catch (const Ice::TwowayOnlyException&){
      cerr << "setPackage() is not oneway" << endl;
    }

    //twoways->getPackage();



    // prx->execute();
    //end package building

    // cout << "nombre: " << prx->getPackage() << endl;




    return 0;
  }
};

int main (int argc, char* argv[]) {
  Client* app = new Client();
  app->main(argc, argv);
  exit(0);
}
