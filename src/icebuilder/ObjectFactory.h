#ifndef OBJECTFACTORY_H
#define OBJECTFACTORY_H

Template<class T>
class ObjectFactory : public Ice::ObjectFactory{
 public:
  virtual Ice::ObjectPtr create(const string &);
  virtual void destroy();

}

#endif
