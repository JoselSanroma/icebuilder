// -*- coding:utf-8; tab-width:4; mode:cpp -*-

/*Connecting to a local hypervisor*/

/* compile with: gcc -g -Wall connect.cpp -o connect -lvirt */
#include <stdio.h>
#include <stdlib.h>
#include <libvirt/libvirt.h>

int main(int argc, char *argv[])
{
    virConnectPtr conn;

    conn = virConnectOpen("qemu:///system");
    if (conn == NULL) {
        fprintf(stderr, "Failed to open connection to qemu:///system\n");
        return 1;
    }
    virConnectClose(conn);
    return 0;
}
