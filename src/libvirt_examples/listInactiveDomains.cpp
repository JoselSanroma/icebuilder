// -*- coding:utf-8; tab-width:4; mode:cpp -*-
/* compile with: gcc -g -Wall listInacetiveDomains.c -o lisItDom -lvirt */

#include <stdio.h>
#include <stdlib.h>
#include <libvirt/libvirt.h>

int main(int argc, char *argv[]){

  virConnectPtr conn;
  int i;
  int numDomains;
  char *inactiveDomains;


  conn = virConnectOpen("qemu:///system");
  if (conn == NULL) {
    fprintf(stderr, "Failed to open connection to qemu:///system\n");
    return 1;
  }

  numDomains = virConnectNumOfDefinedDomains(conn);

  inactiveDomains = malloc(sizeof(char *) * numDomains);
  numDomains = virConnectListDomains(conn, inactiveDomains, numDomains);

  printf("Inactive domain names:\n");
  for (i = 0 ; i < numDomains ; i++) {
    printf("  %d\n", inactiveDomains[i]);
    free(inactiveDomains[i]);
  }
  free(inactiveDomains);

  virConnectClose(conn);

  return 0;
}
