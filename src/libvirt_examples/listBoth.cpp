// -*- coding:utf-8; tab-width:4; mode:cpp -*-
/* compile with: gcc -g -Wall listInacetiveDomains.c -o lisItDom -lvirt */

#include <stdio.h>
#include <stdlib.h>
#include <libvirt/libvirt.h>

int main(int argc, char *argv[]){

  virConnectPtr conn;
  
  virDomainPtr *allDomains;
  int numDomains;
  int numActiveDomains, numInactiveDomains;
  char *inactiveDomains;
  int *activeDomains;
  int i;
  numActiveDomains = virConnectNumOfDomains(conn);
  numInactiveDomains = virConnectNumOfDefinedDomains(conn);

  allDomains = malloc(sizeof(virDomainPtr) * numActiveDomains + numInactiveDomains);
  
  inactiveDomains = malloc(sizeof(char *) * numDomains);

  activeDomains = malloc(sizeof(int) * numDomains);

  numActiveDomains = virConnectListDomains(conn,
                                           activeDomains,
                                           numActiveDomains);
  numInactiveDomains = virConnectListDomains(conn,
                                             inactiveDomains,
                                             numInactiveDomains);

  
  for (i = 0 ; i < numActiveDomains ; i++) {
    allDomains[numDomains] = virDomainLookupByID(activeDomains[i]);
    numDomains++;
  }
  for (i = 0; i < numInactiveDomains ; i++) {
    allDomains[numDomains] = virDomainLookupByName(inactiveDomains[i]);
    free(inactiveDomains[i]);
    numDomains++;
  }
  

  return 0;
}
