#ifndef COMMAND_H
#define COMMAND_H

#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>

using namespace std;

//Class Command
class Command{
public:
  virtual void execute()=0;
};

class SimpleCommand : public Command{
 private:
  string arch;
  string op;
  string name;
 public:
  SimpleCommand();
  SimpleCommand(string a, string o, string n);
  void execute();
};


class Build:public Command{
private:
  string arch, name, snapshot;
  vector<Command*> commands;
public:
  Build();
  Build(string a, string name, string sn);
  //  Build(string vMachine, string packageName, string snapshotName);
  void add(Command* c);
  void execute();
};

#endif
