// -*- coding:utf-8; tab-width:4; mode:cpp -*-

#include "Command.h"


int main(){
  string arch="bully-i386";
  string name="sl";
  string snapshot="snapshot-"+arch;
  vector<Command*> v;
  // Build build;
  // //  Build build(arch,name,snapshot);
  // build.add(new Start(arch));  
  // build.add(new Update(arch));  
  // build.add(new Upgrade(arch));  
  // build.add(new CreateSnapshot(arch,snapshot));  
  // build.add(new SourcePackage(arch, name));  
  // build.add(new BuildDependencies(arch, name));  
  // build.add(new BuildPackage(arch, name));  
  // build.add(new RevertSnapshot(arch,snapshot));  
  // build.add(new DeleteSnapshot(arch,snapshot));  
  // build.add(new TurnOff(arch));
  
  // build.add(new SimpleCommand(arch, name, snapshot));
  Build build(arch, name, snapshot);
  
  
  build.execute();

  return 0;
}
