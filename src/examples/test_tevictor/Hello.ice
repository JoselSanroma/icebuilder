module UCLM {
  
  struct Package{
    string name;
    int version;
  };
  interface Hello {
    ["freeze:write"]
      void puts(string str);
  };
  
  class HelloPersistent implements Hello {
    Package p;
    int useCount;
  };
};

