// -*- mode: C++; coding: utf-8 -*-

#include <Freeze/Freeze.h>
#include <IceUtil/IceUtil.h>
#include <Hello.h>
#include <HelloI.h>

using namespace std;
using namespace UCLM;


class Server: public Ice::Application {
public:
  virtual int run (int argc, char* argv[]) {
    shutdownOnInterrupt();
    communicator()
      ->addObjectFactory(new HelloFactory(), 
			HelloPersistent::ice_staticId());

    cout << "id " << HelloPersistent::ice_staticId() << endl;

    Ice::ObjectAdapterPtr oa = communicator()
      ->createObjectAdapterWithEndpoints("HelloOA", "tcp -p 8889");
    Freeze::EvictorPtr e =
      Freeze::createTransactionalEvictor(oa, "db", "hello");
      // Freeze::createBackgroundSaveEvictor(oa, "db", "hello", new HelloInitializer());
    oa->addServantLocator(e, "");
    for (int i=0; i<5; ++i) {
      char str[] = "hello?";
      str[5]='0' + i;
      Ice::Identity ident = communicator()->stringToIdentity(str);
      // cout << "identity: " << ident << endl;
      if (e->hasObject(ident)) {
	cout << ident.name << " ya estaba en este evictor" << endl;
      }
      else {
	Ice::ObjectPrx obj = e->add(new HelloPersistentI(), ident);
	cout << communicator()->proxyToString(obj) << endl;
      }
    }
    oa->activate();
    communicator()->waitForShutdown();
    cout << "Done" << endl;
    return 0;
  }
};

int main (int argc, char* argv[]) {
  Server* app = new Server();
  app->main(argc, argv);
  exit(0);
}
