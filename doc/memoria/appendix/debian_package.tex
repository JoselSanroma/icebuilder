\chapter{El paquete Debian}
\label{cha:el-paquete-debian}

\drop{E}{l} paquete Debian es una parte muy importante del desarrollo
de este proyecto, de hecho, todo gira en torno a él. Aunque para
documentar todo lo referente al paquete Debian ya está la \emph{Debian
  Policy}~\cite{policy} y la \emph{guía del nuevo desarrollador de
  Debian}~\cite{maintainer}, se mostrará a modo resumido todos los
aspectos considerados más importantes del paquete Debian que pueden
resultar de ayuda para entender el presente documento. No obstante, se
recomienda al lector que quiera conocimientos más profundos leerlos
los documentos citados anteriormente.

Se podría hablar de dos tipos de paquetes Debian, el oficial, que es
el que se encuentra en los repositorios oficiales de Debian, y el no
oficial, que es el que no se encuentra en los repositorios oficiales
de Debian y se puede distribuir por los medios que cada uno considere
oportunos. Un paquete no oficial puede estar construido de la forma
correcta, pero la diferencia es que no ha sido subido a Debian por
ningún «Debian Developer».


\section{Primeros pasos}
\label{sec:primeros-pasos}

Si se va a empaquetar algún programa, lo primero que hay que hacer es obtener una copia
del programa para probarlo. A partir de aquí, la generación de un paquete Debian requiere
nombar a los ficheros de una determinada manera.
Por ejemplo, la copia del programa, comprimo en formato \texttt{tar}:
\texttt{package\_version.tar.gz}

Lo siguiente es añadir las modificaciones para «debianizar» el programa:

\begin{itemize}
\item \texttt{package\_version.orig.tar.gz}
\item \texttt{package\_version-revision.debian.tar.gz}
\item \texttt{package\_version.orig.dsc}
\end{itemize}

Y por último generar el programa: \texttt{package\_version-revision\_arch.deb}

Nótese que la palabra «package» debe ser el nombre del programa en
cuestión y «version» el número de versión del programa
original. «revision» es la revisión del paquete Debian, y por último
«arch» es la arquitectura del paquete según la \emph{Debian
  Policy}~\cite{policy}




\section{Estructura básica}
\label{sec:estructura-basica}

Normalmente los paquetes Debian se construyen a partir de un código
que no está desarrollado por el propio mantenedor del paquete. Es por
ello habitual partir de algún archivo comprimido \emph{tar.gz} donde
se encuentra todo el código fuente. El programa \texttt{dh\_make}
ayuda a generar las plantillas de ficheros necesarios para construir
el paquete. \texttt{dh\_make} necesita conocer la información del
mantenedor, por lo que es necesario proporcionar tanto el nombre
completo (DEBFULLNAME) como el e-mail (DEBEMAIL). Es bueno añadir
estas variables al \texttt{\$HOME/.bashrc} para que no se tengan que
escribir una y otra vez:

\begin{listing}[style = consola, numbers=none]
  DEBFULLNAME="Nombre Apellidos"
  DEBEMAIL="tue-mail@ejemplo.com"
  export DEBULLFNAME DEBEMAIL
\end{listing}

Una vez configurado es hora de ejecutar \texttt{dh\_make} desde dentro
del directorio del programa.
\begin{listing}[style = consola, numbers=none]
  dh_make -f programa.tar.gz

\end{listing}

Tras la ejecución de la orden anterior, se ha crea un directorio
\texttt{debian} con muchos ficheros, muchos de ellos son plantillas.
La «única» tarea que hay que hacer es completar toda la información
requerida siguiendo la «Debian Policy».


\subsection{Archivos necesarios en el directorio \texttt{debian}}
\label{sec:arch-neces-en}

A continuación se describirán los archivos importantes para el
paquete. Cuando se refiera a algún archivo se hará mediante la ruta
relativa al directorio que se acaba de crear con \texttt{dh\_make}, es
decir, si se refiere al archivo \texttt{debian/control} quiere decir
que el archivo estará dentro del directorio \texttt{debian} y se
llamará \texttt{control}. El \texttt{debian/copyright} es el archivo
\texttt{copyright} que está dentro del directorio \texttt{debian} y
así sucesivamente.


\subsubsection{debian/control}
\label{sec:debiancontrol}

Es uno de los ficheros más importantes del paquete \emph{Debian}, es
una especie de manifiesto donde se incluye información relevante para
la el sistema de gestión de paquetes. Véase un archivo
\texttt{debian/control} cualquiera, por ejemplo el del paquete
\emph{gentoo}.

\begin{listing}[style = normal, numbers=none,
  caption= {debian/control paquete Debian},
  label = code:debian_control]

  Source: gentoo
  Section: x11
  Priority: optional
  Maintainer: Innocent De Marchi <tangram.peces@gmail.com>
  Build-Depends: debhelper (>= 9), dh-autoreconf, libgtk2.0-dev, libglib2.0-dev
  Standards-Version: 3.9.3
  Homepage: http://www.obsession.se/gentoo/

  Package: gentoo
  Architecture: any
  Depends: ${shlibs:Depends}, ${misc:Depends}
  Suggests: file
  Description: fully GUI-configurable, two-pane X file manager
  gentoo is a two-pane file manager for the X Window System. gentoo lets the
  user do (almost) all of the configuration and customizing from within the
  program itself. If you still prefer to hand-edit configuration files,
  they're fairly easy to work with since they are written in an XML format.
  .
  gentoo features a fairly complex and powerful file identification system,
  coupled to an object-oriented style system, which together give you a lot
  of control over how files of different types are displayed and acted upon.
  Additionally, over a hundred pixmap images are available for use in file
  type descriptions.
  .
  gentoo was written from scratch in ANSI C, and it utilizes the GTK+ toolkit
  for its interface.

\end{listing}



En este fichero hay dos secciones diferenciadas, el paquete fuente y
la información necesaria para su construcción se pueden ver en la
sección \texttt{Source}. El paquete o los paquetes binarios están
separados por una línea en blanco y están en la sección
\texttt{Package}.

Hágase un repaso de los campos del paquete fuente donde si no se
especifica lo contrario, el campo a rellenar no es obligatorio:

\begin{description}
\item[Source] Es el nombre del paquete fuente y es obligatorio. Tanto
  en el \texttt{debian/control} como en el archivo \texttt{.dsc}, este
  campo debe tener solamente el nombre del paquete fuente.

\item[Maintainer] Nombre del mantenedor del paquete y su dirección de
  correo electrónico, este campo es obligatorio. El nombre aparece
  primero y luego el correo eĺectrónico entre < >.

\item[Uploaders] Nombre o nombres con sus respectivas
  direcciones de correo electrónico de los mantenedores del
  paquete. El formato es el mismo que en el campo anterior.

\item[Section] Este campo no es obligatorio pero si
  recomendable. Especifica un área en la cual clasificar el paquete
  \footnote{Lista completa de las secciones en «SID»
    \href{http://packages.debian.org/unstable/}{http://packages.debian.org/unstable/}}

\item[Priority] Este campo es recomendable y representa el nivel de
  importancia que tiene para el usuario que este paquete esté
  instalado. Estos niveles pueden ser: \texttt{required},
  \texttt{important}, \texttt{standard}, \texttt{optional} ó
  \texttt{extra}. Para ver una descripción detallada deniveles de
  prioridad consultar el capítulo 2.5 de la «Debian
  Policy»~\cite{policy}
%http://www.debian.org/doc/debian-policy/ch-archive.html#s-priorities

\item[Build-depends] Es una lista con los nombres de los paquetes y en
  algunos casos sus versiones que son necesarios para construir el
  paquete. Estos paquetes deben estar instalados en el sistema a la
  hora de construir el paquete fuente para poder construir los
  paquetes binarios.

\item[Standars-Version] Recomendado, la versión más reciente de las
  normas con el que el paquete cumple.

\item[Homepage] La dirección del sitio web para el
  paquete. Preferiblemente la web desde donde puede obtenerse el
  paquete fuente y cualquier documentación que pueda servir de
  ayuda. El contenido desde este campo es \textbf{solamente} la URL,
  sin nada más.

\item[Vcs-Browser] Los paquetes están desarrollados con controles de
  versiones. El propósito es indicar el repositorio público donde se
  está desarrollando el paquete fuente.

\end{description}



Campos del paquete binario:

\begin{description}
\item[Package] Campo obligatorio en el que se pone el nombre del
  paquete binario, el que dará lugar al \texttt{.deb}. Este nombre
  deberá cumplir una serie de reglas dependiendo del lenguaje en que
  está escrito, su finalidad, etc.

\item[Architecture] Dependiendo del contexto, este campo obligatorio,
  puede incluir una de las siguiente opciones:
  \begin{itemize}
  \item Una única palabra identificando una de las arquitecturas
    descritas. Estos nombres pueden obtenerse con la orden \texttt{dpkg-architecture -L}.
  \item Una arquitectura comodín como puede ser \texttt{any} y que
    sirve para todas las arquitecturas listadas con la orden
    \texttt{dpkg-architecture -L}. Es la más usada.
  \item \texttt{all} lo que indica que el paquete es independiente de
    la arquitectura. Para que el lector se haga una idea, \texttt{all}
    se refiere a programas que funcionan en todas las arquitecturas
    tales como ficheros multimedia, manuales  o programas escritos en
    lenguajes interpretados, etc.
  \item \texttt{source} lo cual indica un paquete fuente.
  \end{itemize}

\item[Section] Este campo aunque no es obligatorio, si es
  recomendable. Tal y como en el paquete fuente, indica un área en
  donde ubicar le paquete.

\item[Priority] Recomendado. Indica como de importante para el usuario
  es tener el paquete instalado. La prioridad \texttt{optional} se utiliza para paquetes
  nuevos que no entran en conflicto con otros de prioridad \texttt{required},
  \texttt{important} o \texttt{standar}

\item[Depends y otras] Son las dependencias binarias del paquete que
  describen sus relaciones con el resto de paquete. Básicamente son
  los paquete que se requiere que estén instalados en el sistema para
  que el paquete funcione. Una relación de las dependencias binarias
  puede verse en el capítulo 7.2 de la \emph{Debian
    policy}~\cite{policy}

\item[Description] Este campo es obligatorio. Contiene la descripción
  del paquete binario. Consiste en dos parte, una descripción corta y
  la descripción larga basándose en el siguiente formato y sin los
  símbolos <>.
% \begin{listing}[style = consola, numbers=none]
%   Description: $\textlptruna$sola línea$\textrptr$
%   $\textlptruna$descripción larga con más de una línea$\textrptr$
% \end{listing}

\item[Homepage] La dirección del sitio web para el
  paquete. Preferiblemente la web desde donde puede obtenerse el
  paquete fuente y cualquier documentación que pueda servir de
  ayuda. El contenido desde este campo es \textbf{solamente} la URL,
  sin nada más.

\item[Package-Type] Campo simple donde se indica el tipo de paquete,
  \texttt{deb} para paquetes binarios y \texttt{udeb} para paquete
  micro binarios.


\end{description}


\subsubsection{debian/changelog}
\label{sec:debianchangelog}

Este fichero contiene una descripción muy breve de los cambios que el
mantenedor del paquete hace a los ficheros específicos del paquete. No
se describen los cambios que sufre el programa, eso corresponde a su
autor y eso suele estar en un fichero \texttt{.changes}

\begin{listing}[style = normal, numbers=none,
  caption= {changelog paquete Debian},
  label = code:debian_changelog]

  miproyecto (1.0-1) unstable; urgency=low

  * Initial release (Closes: #nnnn)  <nnnn is the bug number of your ITP>

 -- John Doe <johnDoe@icebuilder.com>  Wed, 25 Dec 2013 14:02:25 +0100


\end{listing}



Cada vez que el mantenedor realice un cambio en el paquete debe crear
una nueva entrada en el \texttt{debian/changelog}. Si el mantenedor
hace cambio en la versión del paquete, se incrementará el número de
versión del paquete. Cuando el mantenedor del paquete empaquete una
nueva versión, el número de versión del paquete vuelve a empezar desde
1.

En l listado \ref{code:debian_changelog} «miproyecto» es el nombre del
paquete, y entre parentesis (1.0-1), del cual «1.0» es la versión del
código fuente, mientras que «1» es la versión del paquete Debian.

\subsubsection{debian/copyright}
\label{sec:debiancopyright}

Este fichero contiene información sobre el autor del programa y las
licencias que se utilizan en el programa y en cada una de sus
partes. Debería indicar también quien es el autor y la licencia de los
ficheros del paquete Debian.



\begin{listing}[style = normal, numbers=none,
  caption= {copyright paquete Debian},
  label = code:debian_copyright]

  Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
  Upstream-Name: miproyecto
  Source: <url://example.com>

  Files: *
  Copyright: <years> <put author's name and email here>
  <years> <likewise for another author>
  License: GPL-3.0+

  Files: debian/*
  Copyright: 2013 John Doe <johnDoe@icebuilder.com>
  License: GPL-3.0+

  License: GPL-3.0+
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  .
  This package is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  .
  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>.
  .
  On Debian systems, the complete text of the GNU General
  Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

  # Please also look if there are files or directories which have a
  # different copyright/license attached and list them here.
  # Please avoid to pick license terms that are more restrictive than the
  # packaged work, as it may make Debian's contributions unacceptable upstream.
\end{listing}


\subsubsection{debian/rules}
\label{sec:debianrules}

Es el \texttt{Makefile} que debe inlcuir una serie de objetivos como : clean, binary,
binary-arch, binary-indep y build.

Estas reglas objetivo serán ejecutadas por \texttt{dpkg-buildpackage} o \texttt{debuild}
cuando se construya el paquete.

\begin{itemize}
\item \texttt{clean} (obligatorio): elimina todos los archivos generados, compilados o
  innecesarios del árbol de directorios de las fuentes.

\item \texttt{build} (obligatorio): para la construcción de archivos compilados a partir
  de los archivos fuenteo o la construcción de documentos formateados.

\item objetivo \texttt{build-arch} (obligatorio): para la compilación de las fuentes en
  programas compilados (dependientes de la arquitectura) en el árbol
  de directorios de la compilación.

\item objetivo \texttt{build-indep} (obligatorio): para la compilación de las fuentes en
  documentos formateados (independientes de la aqruitectura) en el árbol de directorios de
  la compilación

\item \texttt{install} (opcional): para la instalación en la estructura de directorios
  temporal para el directorio \texttt{debian} de los archivos para cada uno de los
  paquetes binarios. Si existe el objetivo \texttt{binary} dependerá
  de este.

\item \texttt{binary} (obligatorio): para la construcción de cada uno de los paquetes
  binarios (combinando con los objetivos \texttt{binary-arch} y
  \texttt{binary-indep}).

\item \texttt{binary-arch} (obligatorio): para la construcción de paquetes dependientes de
  la aquitectura (\texttt{Architecture: any}).

\item \texttt{binary-indep} (obligatorio): para la construcción de paquetes independientes de
  la aquitectura (\texttt{Architecture: all}).

\item \texttt{get-orig-source} (opcional): para obtener la versión más reciente de las
  fuentes originales desde el lugar de almacenaje del autor.

\end{itemize}

Cuando se utiliza \texttt{dh\_make} se genera un \texttt{debian/rules} como el siguiente:
\begin{listing}[style = normal, numbers=none,
  caption= {debian/rules paquete Debian},
  label = code:debian_rules]
  #!/usr/bin/make -f
  # -*- makefile -*-

  # Uncomment this to turn on verbose mode.
  #export DH_VERBOSE=1

  % :
        dh $@

\end{listing}

\emph{debhelper} hace casi todo el trabajo aplicando las reglas por defecto por lo que la
única tarea que habría que realizar sería la de sobreescribir dichos objetivos.


\section{La construcción del paquete}
\label{sec:construccion}


Para construir un paquete, primero hay que asegurarse de que estén instalados:
\begin{itemize}
\item el paquete \texttt{build-essential}
\item los paquetes listados en el campo \texttt{build-Depends} del \texttt{debian/control}
\item los paquetes listados en el campo \texttt{build-Depends-Indep} del \texttt{debian/control}
\end{itemize}

Después se ejecuta la orden
\begin{listing}[style = consola, numbers=none]
  $ dpkg-buildpackage
\end{listing}

Para los paquetes no nativos Debian, en el directorio padre se podrán ver los archivos

\begin{itemize}
\item \texttt{paquete\_1.0.orig.tar.gz}
  El código fuente original comprimido, se ha renombrado a ese nombre para seguir los
  estándares de Debian.

\item \texttt{paquete\_1.0-1.dsc}
  Es un resumen de los contenidos del paquete. Se genera a partir del
  \texttt{debian/control} y se usa cuando se descomprimen las fuentes con
  \texttt{dpkg-source}. Está firmado con GPG por lo que se puede saber de quien es el
  paquete.

\item \texttt{paquete\_1.0-1.debian.tar.gz}
  Este fichero contiene el directorio \texttt{debian} al completo. Todas las moficiaciones
  están en los archivos de parches \textbf{quilt} en el directorio \texttt{debian/patches}

\item \texttt{paquete\_1.0-1\_i386.deb}
  Es el paquete binario completo. Se puede instalar o eliminar con \texttt{dpkg} como el
  resto de paquetes.

\item \texttt{paquete\_1.0-1\_i386.changes}
  Este fichero describe todos los cambios hechos en la versión actual del paquete y lo
  utilizan algunos programas de \acs{FTP} de Debian



\end{itemize}

\section{Parches}
\label{sec:parches}
TODO and move


\section{Actualización del paquete}
\label{sec:actu-del-paqu}

Es posible que el paquete deba ser actualizado por diversos motivos.

\subsection{Nueva versión del paquete}
\label{sec:nueva-version-del}


Tras tener el paquete listo y dado que muchos usuarios lo están utilizando, pueden surgir
bugs. Para modificar el paquete se utiliza la herramienta \texttt{dquilt}. Para aplicar
una nueva modificación:

\begin{listing}[style = consola, numbers=none]
  dquilt new nombre_modificacion.patch
\end{listing}

Y para actualizar una modificación ya existente:

\begin{listing}[style = consola, numbers=none]
  dquilt pop nombre_modificacion.pathc
\end{listing}

Uan descripción más detallada de la orden \texttt{dquilt} puede encontrarse en el capítulo
9 de la \emph{guía del nuevo mantenedor de Debian}~\cite{maintainer}


\subsection{Nueva versión del autor}
\label{sec:nueva-version-del-autor}
Cuando el autor libera una nueva versión, el mantenedor del paquete, debe revisarla. Es
importante ver los \texttt{changelogs}, \emph{NEWS}, ... para saber que es lo que ha
cambiado. Con la orden \texttt{diff} se puede ver rapidamente comparando los ficheros de
la nueva versión con la del antiguo.


\subsection{Nueva versión del programa fuente}
\label{sec:nueva-version-del-programa-fuente}

Si se parte de un paquete bien empaquetado, habría que copiar el directorio
\texttt{debian} de la versión anterior a la nueva para realizar las adaptaciones
necesarias. Además de ello, habría que realizar algunas tareas como comprimir las fuentes
en un nuevo \texttt{paquete\_nueva\_version.orig.tar.gz}, actualizar el \texttt{changelog},
etc. Nuevamente hay más detalles sobre esto en el capítulo 9.4 de la \emph{guía del nuevo
  mantenedor de Debian}\~cite{maintainer}




% Local Variables:
%   coding: utf-8
%   fill-column: 90
%   mode: flyspell
%   ispell-local-dictionary: "castellano8"
%   mode: latex
%   TeX-master: "main"
% End:
