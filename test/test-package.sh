#!/bin/bash --
# -*- coding:utf-8; tab-width:4; mode:shell-script -*-

USER="arco"
DIR="/home/arco/"
HOST="192.168.122.11"

PACKAGE=$2

arch=i386
if [ "$1" = "amd64" ]
then
    arch=amd64
	HOST="192.168.122.12"
fi


function deleteDir () {
    if [ -d builtPackage/ ]
    then
        rm -rf builtPackage
    else
        echo "builtPackage does not exist, nothing to do..."
    fi
}

function testPackage () {

	mkdir -p builtPackage
    echo $HOST
    echo $DIR

	sshpass -p 'arco' scp -o StrictHostKeyChecking=no $USER@$HOST:$DIR/*.deb builtPackage/
	# if [ -e buildPackage/*.deb ]
	# then
	# 	echo [ OK ]
	# else
	# 	echo [ FAIL ]
	# fi

}

function buildPackage () {

    ./../src/scripts/builder.sh $arch start
    ./../src/scripts/builder.sh $arch update
    ./../src/scripts/builder.sh $arch upgrade
    ./../src/scripts/builder.sh $arch createSnapshot test-$arch
    ./../src/scripts/builder.sh $arch sourcePackage $PACKAGE

    #ls -d sl-*
    #ls *.debian.tar.gz
    #ls *.dsc
    #ls *.orig.tar.gz
    ./../src/scripts/builder.sh $arch buildDependencies $PACKAGE
    ./../src/scripts/builder.sh $arch buildPackage $PACKAGE

    testPackage

    ./../src/scripts/builder.sh $arch revertSnapshot test-$arch
    ./../src/scripts/builder.sh $arch deleteSnapshot test-$arch
    ./../src/scripts/builder.sh $arch turnOff


}

deleteDir
buildPackage
